/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package core

import (
	"context"
	"github.com/google/uuid"
	"time"
)

const (
	AuthorizationPayloadKey = "authorization_payload"
)

type (
	UserLoginRequest struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	User struct {
		UserName string    `json:"username"`
		Password string    `json:"password,omitempty"`
		FullName string    `json:"full_name"`
		Email    string    `json:"email"`
		UpdateAt time.Time `json:"-"`
		CreateAt time.Time `json:"-"`
	}

	Session struct {
		ID           uuid.UUID `json:"id"`
		Username     string    `json:"username"`
		RefreshToken string    `json:"refresh_token"`
		UserAgent    string    `json:"user_agent"`
		ClientIP     string    `json:"client_ip"`
		IsBlocked    bool      `json:"is_blocked"`
		ExpiresAt    time.Time `json:"expires_at"`
		CreateAt     time.Time `json:"create_at"`
	}

	RenewAccessTokenRequest struct {
		RefreshToken string `json:"refresh_token"`
	}

	UserStore interface {
		Create(ctx context.Context, user *User) (*User, error)
		Find(ctx context.Context, username string) (*User, error)
		CreateSession(ctx context.Context, session *Session) (*Session, error)
		GetSession(ctx context.Context, id uuid.UUID) (*Session, error)
	}
)
