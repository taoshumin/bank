/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package core

import (
	"context"
	"time"
)

type (
	Account struct {
		ID        int64     `json:"id"`
		Owner     string    `json:"owner"`
		Balance   int64     `json:"balance"`
		Currency  string    `json:"currency"`
		CreatedAt time.Time `json:"created_at"`
	}

	CreateAccountRequest struct {
		Currency string `json:"currency"`
	}

	GetAccountRequest struct {
		ID int64 `uri:"id"`
	}

	ListAccountRequest struct {
		Page int64 `json:"page" form:"page"`
		Size int64 `json:"size" form:"size"`
	}

	FilterParams struct {
		Owner  string
		Limit  int64
		Offset int64
	}

	AccountStore interface {
		Create(ctx context.Context, account Account) (*Account, error)
		Get(ctx context.Context, id int64) (*Account, error)
		List(ctx context.Context, filter FilterParams) ([]*Account, error)
	}
)
