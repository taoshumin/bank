/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"test.io/bank/core"
	"test.io/bank/encryp"
)

// createAccount 创建账户.
func (s *Server) createAccount(ctx *gin.Context) {
	req := new(core.CreateAccountRequest)
	if err := ctx.ShouldBindJSON(req); err != nil {
		return
	}

	authPayload := ctx.MustGet(core.AuthorizationPayloadKey).(*encryp.Payload)
	account, err := s.accountStore.Create(ctx, core.Account{
		Balance:  0,
		Currency: req.Currency,
		Owner:    authPayload.Username,
	})
	if err != nil {
		return
	}

	ctx.JSON(http.StatusOK, account)
}

// getAccount 查看账户.
func (s *Server) getAccount(ctx *gin.Context) {
	req := new(core.GetAccountRequest)
	if err := ctx.ShouldBindUri(req); err != nil {
		return
	}

	account, err := s.accountStore.Get(ctx, req.ID)
	if err != nil {
		return
	}

	if authPayload := ctx.MustGet(core.AuthorizationPayloadKey).(*encryp.Payload); account.Owner != authPayload.Username {
		return
	}
	ctx.JSON(http.StatusOK, account)
}

// listAccounts 账户列表.
func (s *Server) listAccounts(ctx *gin.Context) {
	req := new(core.ListAccountRequest)
	if err := ctx.ShouldBindQuery(req); err != nil {
		return
	}

	authPayload := ctx.MustGet(core.AuthorizationPayloadKey).(*encryp.Payload)
	list, err := s.accountStore.List(ctx, core.FilterParams{
		Owner:  authPayload.Username,
		Limit:  req.Size,
		Offset: (req.Page - 1) * req.Size,
	})
	if err != nil {
		return
	}
	ctx.JSON(http.StatusOK, list)
}
