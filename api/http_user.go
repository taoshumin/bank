/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package api

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"test.io/bank/core"
	"test.io/bank/render"
	"time"
)

// createUser 创建用户
func (s *Server) createUser(ctx *gin.Context) {
	req := new(core.User)
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.JSON(http.StatusBadRequest, render.Error(err))
		return
	}

	hash, err := s.passwordSvc.Generate(req.Password)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	req.Password = hash
	usr, err := s.userStore.Create(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}
	ctx.JSON(http.StatusOK, usr)
}

type loginResponse struct {
	*core.User
	SessionID             uuid.UUID `json:"session_id"`
	AccessToken           string    `json:"access_token"`
	AccessTokenExpiresAt  time.Time `json:"access_token_expires_at"`
	RefreshToken          string    `json:"refresh_token"`
	RefreshTokenExpiresAt time.Time `json:"refresh_token_expires_at"`
}

// Login 用户登录.
func (s *Server) Login(ctx *gin.Context) {
	req := new(core.UserLoginRequest)
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.JSON(http.StatusBadRequest, render.Error(err))
		return
	}

	usr, err := s.userStore.Find(ctx, req.Username)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	if err := s.passwordSvc.Verify(req.Password, usr.Password); err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	accessToken, accessPayload, err := s.tokenizer.Create(usr.UserName, s.accessTokenDuration)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	refreshToken, refreshPayload, err := s.tokenizer.Create(usr.UserName, s.refreshTokenDuration)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	session, err := s.userStore.CreateSession(ctx, &core.Session{
		ID:           refreshPayload.ID,
		Username:     usr.UserName,
		RefreshToken: usr.Password,
		UserAgent:    ctx.Request.UserAgent(),
		ClientIP:     ctx.ClientIP(),
		IsBlocked:    false,
		ExpiresAt:    refreshPayload.ExpiredAt,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	ctx.JSON(http.StatusOK, loginResponse{
		User:                  usr,
		SessionID:             session.ID,
		AccessToken:           accessToken,
		AccessTokenExpiresAt:  accessPayload.ExpiredAt,
		RefreshToken:          refreshToken,
		RefreshTokenExpiresAt: refreshPayload.ExpiredAt,
	})
}

type renewAccessTokenResponse struct {
	AccessToken          string    `json:"access_token"`
	AccessTokenExpiresAt time.Time `json:"access_token_expires_at"`
}

// renewAccessToken 刷新访问令牌
func (s *Server) renewAccessToken(ctx *gin.Context) {
	var req core.RenewAccessTokenRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, render.Error(err))
		return
	}

	refreshPayload, err := s.tokenizer.Verify(req.RefreshToken)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	session, err := s.userStore.GetSession(ctx, refreshPayload.ID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	if session.IsBlocked {
		ctx.JSON(http.StatusInternalServerError, render.Error(errors.New("blocked session")))
		return
	}

	if session.Username != refreshPayload.Username || session.RefreshToken != req.RefreshToken {
		ctx.JSON(http.StatusInternalServerError, render.Error(errors.New("mismatched session token")))
		return
	}

	if err := refreshPayload.Valid(); err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	accessToken, accessPayload, err := s.tokenizer.Create(refreshPayload.Username, s.accessTokenDuration)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, render.Error(err))
		return
	}

	ctx.JSON(http.StatusOK, renewAccessTokenResponse{
		AccessToken:          accessToken,
		AccessTokenExpiresAt: accessPayload.ExpiredAt,
	})
}
