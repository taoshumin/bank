/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package api

import (
	"github.com/gin-gonic/gin"
	"test.io/bank/core"
	"test.io/bank/encryp"
	"time"
)

type Server struct {
	passwordSvc  core.PasswordService
	userStore    core.UserStore
	accountStore core.AccountStore
	tokenizer    encryp.Tokenizer

	accessTokenDuration  time.Duration
	refreshTokenDuration time.Duration
}

func (s *Server) Router() *gin.Engine {
	r := gin.Default()

	// 授权认证模块.
	r.POST("/user", s.createUser)
	r.POST("/login", s.Login)
	r.POST("renew_access", s.renewAccessToken)

	// 账户模块.
	ag := r.Group("/").Use(SetMiddlewareAuth(s.tokenizer))
	ag.POST("/accounts", s.createAccount)
	ag.GET("/accounts/:id", s.getAccount)
	ag.GET("/accounts", s.getAccount)

	// 转账模块.
	ag.POST("/transfers", s.createTransfer)
	return r
}
