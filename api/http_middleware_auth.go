/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package api

import (
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"test.io/bank/core"
	"test.io/bank/encryp"
	"test.io/bank/render"
)

func SetMiddlewareAuth(tokenizer encryp.Tokenizer) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authorizationHeader := ctx.GetHeader("authorization")
		filed := strings.Fields(authorizationHeader)
		authType := strings.ToLower(filed[0])
		if len(authorizationHeader) == 0 || len(filed) < 2 || authType != "bearer" {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, render.Error(errors.New("authorization header is not provided")))
			return
		}

		authToken := filed[1]
		payload, err := tokenizer.Verify(authToken)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, render.Error(err))
			return
		}
		ctx.Set(core.AuthorizationPayloadKey, payload)
		ctx.Next()
	}
}
